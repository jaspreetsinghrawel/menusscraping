# We will have to get an authentication bearer in order to use their API. To get the token we will first need a client_id:

import requests
from bs4 import BeautifulSoup
import re
import json
import pgeocode


# get client_id for grubhub session

session = requests.Session()

static = 'https://www.grubhub.com/eat/static-content-unauth?contentOnly=1'
soup = BeautifulSoup(session.get(static).text, 'html.parser')
client = re.findall("beta_[a-zA-Z0-9]+", soup.find('script', {'type': 'text/javascript'}).string)
print(client)



# get authentication bearer
# define and add a proper header
headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
        'authorization': 'Bearer',
        'content-type': 'application/json;charset=UTF-8'
          }
session.headers.update(headers)

# straight from networking tools. Device ID appears to accept any 10-digit value
data = '{"brand":"GRUBHUB","client_id":"' + client[0] + '","device_id":9934567890,"scope":"anonymous"}'
resp = session.post('https://api-gtm.grubhub.com/auth', data=data)

# refresh = json.loads(resp.text)['session_handle']['refresh_token']
access = json.loads(resp.text)['session_handle']['access_token']

# update header with new token
session.headers.update({'authorization': 'Bearer ' + access})

# Instantiate the geocode object to find out the latitude and longitude for a zip code in the US.
postalcode = "11214"
nomi = pgeocode.Nominatim('us')
location = nomi.query_postal_code(postalcode)


# make your request
# Here comes the part that is different from your request: their API uses a third-party service to get the longitude and latitude for a ZIP code (i.e. -73.99916077, 40.75368499) for your New York ZIP code). There might even be an option to change that: location=POINT(-73.99916077%2040.75368499) looks like it accepts other options, as well.

# API for DELIVERY restaurants menu
#grub = session.get('https://api-gtm.grubhub.com/restaurants/search/search_listing?orderMethod=delivery&locationMode=DELIVERY&facetSet=umamiV2&pageSize=20&hideHateos=true&searchMetrics=true&location=POINT({}%20{})&facet=promos%3Atrue&facet=open_now%3Atrue&sortSetId=umamiv3&countOmittingTimes=true'.format(location['longitude'], location['latitude']))

# API for PICKUP restaurants menu
grub = session.get('https://api-gtm.grubhub.com/restaurants/search/search_listing?orderMethod=pickup&locationMode=PICKUP&facetSet=umamiV2&pageSize=20&hideHateos=true&searchMetrics=true&location=POINT({}%20{})&facet=promos%3Atrue&facet=open_now%3Atrue&sortSetId=umamiv3&countOmittingTimes=true'.format(location['longitude'], location['latitude']))

# grub = session.get('https://api-gtm.grubhub.com/restaurants/search/search_listing?orderMethod=delivery&locationMode=DELIVERY&facetSet=umamiV2&pageSize=20&hideHateos=true&searchMetrics=true&location=POINT(-73.999)&facet=promos%3Atrue&facet=open_now%3Atrue&sortSetId=umamiv3&countOmittingTimes=true')

print (grub.text)



